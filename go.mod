module gitlab.com/ivinci/ivijson

go 1.13

require (
	cloud.google.com/go v0.69.1
	gitlab.com/jhelberg/frog v0.0.0-20200415210011-214ca5b0f449
	gitlab.com/jhelberg/rasa v0.0.0-20200721211742-a18e1f49b9ea
	google.golang.org/genproto v0.0.0-20201014134559-03b6142f0dc9
)
