package ivijson

 import (
       "log"
       "os"
       "bufio"
       "strings"
       "regexp"
       "errors"
       "encoding/json"
       "encoding/hex"
       "encoding/base64"
       "time"
       "bytes"
       "image"
       "image/jpeg"
       "image/png"
       "gitlab.com/jhelberg/frog"
       "gitlab.com/jhelberg/rasa"
       visionpb "google.golang.org/genproto/googleapis/cloud/vision/v1"
       vision "cloud.google.com/go/vision/apiv1"
 )

 type Sentiment struct {
   Label string   `json:"label,omitempty"`     // one of negative, neutral, positive
   Score float64  `json:"score,omitempty"`     // number between 0 and 1 (negative…positive)
 }
 type SttWord struct {
   Word     string  `json:"word,omitempty"`
   Starting time.Duration `json:"starting,omitempty"`
   Ending   time.Duration `json:"ending,omitempty"`
 }
 type SpeechToText struct {
   Words      []SttWord `json:"words,omitempty"`
   Confidence float32   `json:"confidence,omitempty"`
   Ending     time.Duration `json:"ending,omitempty"`
 }
 type Tout struct {
   Target   string `json:"target,omitempty"`   // speech actor (can be a display, a LED or a loudspeaker) …
   Text     string `json:"text,omitempty"`     // the text to be brought across to the users
   Starting string `json:"starting,omitempty"` // the point in time from the bot's viewpoint
   Audiolog string `json:"audiolog,omitempty"` // filename if audio, to be filled in by speak-program
   Type     string `json:"type,omitempty"`     // type as part of the mimetype of the Text field (text/application?)
   Subtype  string `json:"subtype,omitempty"`  // subtype as part of the mimetype of the Text field (plain/html/msword etc.)
   Encoding string `json:"encoding,omitempty"` // how the Text field is encoded (well, utf-8 of course)
 }
 type Iout struct {
   Target   string `json:"target,omitempty"`   // display actor (can be a display, printer, LED)…
   Image    string `json:"image,omitempty"`    // the base64-encoded image to output
   Starting string `json:"starting,omitempty"` // the point in time from the bot's viewpoint
   Videolog string `json:"videolog,omitempty"` // filename if video, to be filled in by display-program
   Type     string `json:"type,omitempty"`     // type as part of the mimetype of the Image field (image/video etc.)
   Subtype  string `json:"subtype,omitempty"`  // subtype as part of the mimetype of the Image field (png?)
   Encoding string `json:"encoding,omitempty"` // is this applicable for images?
 }
 type Cout struct {
   Target   string `json:"target,omitempty"`   // display actor (can be a display, printer, LED)…
   Command  string `json:"command,omitempty"`  // the command to execute
   Starting string `json:"starting,omitempty"` // the point in time from the bot's viewpoint
   Cntrllog string `json:"videolog,omitempty"` // filename if video, to be filled in by display-program
   Subtype  string `json:"subtype,omitempty"`  // dont think this applies to control
   Type     string `json:"type,omitempty"`     // dont think this applies to control
   Encoding string `json:"encoding,omitempty"` // how the Command field is encoded (well, utf-8 of course)
 }
 const Version string = "0.11"
     // 0.4 adds textcl, 0.5 adds control, 0.6 adds Imageout,
     // 0.7 uses omitempty and removes dependency on google STT
     // 0.8 adds redoid and control commands
     // 0.9 adds imageenc for image encoding
     // 0.10 adds control out and mimetypes for outgoing content
     // 0.11 adds superevent for linked events
 type Inputformat struct {
   Producer string               `json:"producer"`
   Version  string               `json:"version"`
   Redoid   int32                `json:"redoid"`              // used by the api to re-inject data
   Participant string            `json:"participant"`
   Session  string               `json:"session"`
   Boxid    string               `json:"boxid"`
   Model    string               `json:"model"`
   Muxtag   string               `json:"muxtag"`
   Starting string               `json:"starting"`
   Utterance string              `json:"utterance,omitempty"` // also in Stt, but Stt goes away in the end
   Stt      SpeechToText         `json:"stt,omitempty"`
   Grm      []frog.Token         `json:"grm,omitempty"`
   Intent   rasa.IntentContainer `json:"intent,omitempty"`
   Snt      Sentiment            `json:"snt,omitempty"`
   Audiolog string               `json:"audiolog,omitempty"`  // some trace of an audio file PCM-encoded as below
   Aencoding string              `json:"aencoding,omitempty"` // e.g. S16_LE
   Asrate   int32                `json:"asrate,omitempty"`    // e.g. 32000
   Videolog string               `json:"videolog,omitempty"`  // some trace of a video file
   Textout  Tout                 `json:"tts,omitempty"`       // text to speech
   Imageout Iout                 `json:"itd,omitempty"`       // image to display
   Cntrlout Cout                 `json:"cout,omitempty"`      // control out
   Control  string               `json:"control,omitempty"`   // control commands for programs in the pipe-line
   Params   []string             `json:"params,omitempty"`    // control command parameters
   Posture  []float64            `json:"posture,omitempty"`   // posture indicator
   Sift     []float64            `json:"sift,omitempty"`      // scale invariant face tag (128-item vector)
   Micexp   []float64            `json:"micexp,omitempty"`    // micro expresions vector
   Portrait string               `json:"portrait,omitempty"`  // basis for sift and micexp
   Image    string               `json:"image,omitempty"`     // basis for posture
   Imagest  string               `json:"imagest,omitempty"`   // subtype of image, the part behind the / of the mimetype image/…
   Imageenc string               `json:"imageenc,omitempty"`  // encoding of image: hex, base64, filename etc.
   Nfaces   int                  `json:"nfaces,omitempty"`    // cardinality of faces in Image
   Nthface  int                  `json:"nthface,omitempty"`   // which of that is this message about, start with biggest first: 0
   Superevent int                `json:"superevent,omitempty"`// some events have a hierarchical relationship with other events
   Expressions map[string]int    `json:"expressions,omitempty"`
   Audiodevice string            `json:"audiodevice,omitempty"` // input or output channel for this audio
   Videodevice string            `json:"videodevice,omitempty"` // input or output channel for this video
   Cntrldevice string            `json:"cntrldevice,omitempty"` // input or output channel for this control command
   TextCL   string               `json:"textcl,omitempty"`      // comprehension level: A1, A2, B1, B2, C1, C2
 }
 type Outputformat = Inputformat

 var ErrInvalidJson = errors.New( "Invalid json" )


 // some functions for copying and converting
 // this should check for inputformat version and code version, if different, signal a warning!
 func (sobj *Inputformat) Copytoout() Outputformat {
   if sobj.Version != Version {
     log.Printf( "(%s:ivijson) receiving version %s from %s, but running version %s", os.Args[0], sobj.Version, sobj.Producer, Version)
   }
   out := *sobj
   return out
 }
 func Unmarshal( theline string ) ( Inputformat, error ) {
   var input Inputformat
   if !json.Valid( []byte( theline ) ) {
      return input, ErrInvalidJson
   }
   err := json.Unmarshal( []byte( theline ), &input )
   if (len( input.Image ) > 0 || len( input.Portrait ) > 0) && input.Imageenc == "" {
     input.Imageenc = "hex"
   }
   return input, err
 }
 func (sobj *Outputformat) Marshal() ( string, error ) {
   sobj.Version = Version
   outputstr, err := json.Marshal( *sobj )
   return string( outputstr ), err
 }
 var stdioreader = bufio.NewReader( os.Stdin )
 func ReadInput() (string, error) {
   return ReadLine( stdioreader )
 }
// func ReadInput() (string, error) {
//   return ReadInputFile( os.Stdin )
// }
 func ReadLine( r *bufio.Reader ) (string, error) {
   ret, err := r.ReadString( '\n' )
   if err != nil {
     ret = strings.TrimRight( ret, "\n" )
     return ret, err
   }
   return ret, err
 }

 // maybe read until end of JSON message is better?
 func ReadInputFile( in *os.File ) (string, error) {
   var result []byte
   terminator := byte( 0xa )
   buf := make( []byte, 1 )
   for buf[ 0 ] != terminator {
      if _, err := in.Read( buf ); err != nil {
         return "", err
      }
      result = append( result, buf[ 0 ] )
   }
   return string( result ), nil
 }

 // an important objective of the audiopipeline: the transcript
 func (sobj *Inputformat) Transcript() string {
   return sobj.Utterance
 }

 func (sobj *Inputformat) IsAudio() bool {
    return sobj.Audiolog != "" || sobj.Utterance != "" ||
           len( sobj.Stt.Words ) != 0
 }
 func (sobj *Inputformat) IsVideo() bool {
    return sobj.Videolog != "" || len( sobj.Sift ) > 0 ||
           len( sobj.Image ) > 0 || len( sobj.Portrait ) > 0
 }
 func (sobj *Inputformat) IsCntrl() bool {
    return sobj.Control != "" || len( sobj.Params ) > 0
 }

 // some utility-functions for keeping the data in sync
 func (sobj *Outputformat) TokenizeSTTWords( verbose bool ) Outputformat {
    var synced Outputformat
    synced = *sobj
    synced.Stt.Words = splitWordsAndPunctuation( sobj.Stt.Words, verbose )
    return synced
 }
 // some words contain both words and punctuation, we make one, two or three words of it
 // so  "A[[:punct:]]B" comes out as "A" "punct-string" "B"
 func splitWordsAndPunctuation( words []SttWord, verbose bool ) []SttWord {
   punc := regexp.MustCompile( "[!+,./:;?@{-~€$%]" )
   var newlist []SttWord
   for _, word := range words {
      splitted := splitWord( punc, word, verbose )
      newlist = append( newlist, splitted... )
   }
   return newlist
 }

 // note splitWord is recursive, there is a potential DOS-issues in case the amount
 // of punctuation in a single word exhausts the stack-depth
 func splitWord( re *regexp.Regexp, word SttWord, verbose bool ) []SttWord {
   var newlist []SttWord
   if verbose {
     log.Printf( "(%s:ivijson)Splitting word %v\n", os.Args[0], word )
   }
   position := re.FindStringIndex( word.Word )
   if position == nil {
     return append( newlist, word )
   } else {
     pword := word
     pword.Word = word.Word[ position[0]:position[1] ]
     pword.Starting = word.Ending
     pref := word
     pref.Word = word.Word[ :position[0] ]
     suff := word
     suff.Word = word.Word[ position[1]: ]
     if pref.Word != "" {
        newlist = append( newlist, pref )
     }
     newlist = append( newlist, pword )
     if suff.Word != "" { // the suffix may contain another occurence of re, hence recursive call
        newlist = append( newlist, splitWord( re, suff, verbose )... )
     }
   }
   return newlist
 }

 // 'member'-functions Set and Add:
 func (sobj *Outputformat) SetVersion( version string ) string {
   sobj.Version = version
   return sobj.Version
 }
 func (sobj *Outputformat) AddProducer( name string ) string {
   if name != "" { // don't add if nothing to add
     if sobj.Producer == "" {
       sobj.Producer = name
     } else {
       sobj.Producer = sobj.Producer + "|" + name
     }
   }
   return sobj.Producer
 }
 func (sobj *Outputformat) AddMuxtag( name string ) string {
   if sobj.Muxtag != name && name != "" { // dont add if exactly this name already there, don't add if nothing to add
     if sobj.Muxtag == "" {
       sobj.Muxtag = name // no ":" for the first tag
     } else {
       sobj.Muxtag = sobj.Muxtag + ":" + name
     }
   }
   return sobj.Muxtag
 }

 func (sobj *Outputformat) SetSentiment( label string, score float64 ) {
   sobj.Snt = Sentiment{ Label: label, Score: score, }
   return
 }

 // 'member'-functions Get:
 func (sobj *Inputformat) StartingTime() time.Time {
   t, _ :=time.Parse( time.RFC3339Nano, sobj.Starting ) // 2020-04-02T21:56:23.005756708+02:00
   return t
 }

 func  decodeImage( imagestr string, imagest string, encoding string ) (image.Image, error) {
    decode := hex.DecodeString
    switch encoding {
      case "base64":
        decode = base64.StdEncoding.DecodeString
      case "", "hex":
        decode = hex.DecodeString
      default:
        return nil, errors.New( "Unsupported image encoding" )
    }
    if im, err := decode( imagestr ); err != nil {
       return nil, err
    } else {
      if imagest == "jpg" || imagest == "jpeg" {
        return jpeg.Decode( bytes.NewReader( im ) )
      }
      if imagest == "png" {
        return png.Decode( bytes.NewReader( im ) )
      }
    }
    return nil, errors.New( "Unsupported image format" )
 }

 func decodeImageVI( imagestr string, encoding string ) (*visionpb.Image, error) {
    decode := hex.DecodeString
    switch encoding {
      case "base64":
        decode = base64.StdEncoding.DecodeString
      case "", "hex":
        decode = hex.DecodeString
      default:
        return nil, errors.New( "Unsupported image encoding" )
    }
    if im, err := decode( imagestr ); err != nil {
       return nil, err
    } else {
       return vision.NewImageFromReader( bytes.NewReader( im ) )
    }
 }

 func (sobj *Outputformat) GetPortrait() (image.Image, error) {
    return decodeImage( sobj.Portrait, sobj.Imagest, sobj.Imageenc )
 }
 func (sobj *Outputformat) GetPortraitVI() (*visionpb.Image, error) {
   return decodeImageVI( sobj.Portrait, sobj.Imageenc )
 }
 func (sobj *Outputformat) GetImage() (image.Image, error) {
    return decodeImage( sobj.Image, sobj.Imagest, sobj.Imageenc )
 }
 func (sobj *Outputformat) GetImageVI() (*visionpb.Image, error) {
   return decodeImageVI( sobj.Image, sobj.Imageenc )
 }
